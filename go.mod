module gitee.com/Godfeer/GoBooks

go 1.12

require (
	gitee.com/Godfeer/gotil v0.0.0-20200103025753-1812eb0a6478
	github.com/PuerkitoBio/goquery v1.5.0
	github.com/adamzy/cedar-go v0.0.0-20170805034717-80a9c64b256d // indirect
	github.com/alexcesaro/mail v0.0.0-20141015155039-29068ce49a17
	github.com/aliyun/aliyun-oss-go-sdk v2.0.4+incompatible
	github.com/andybalholm/cascadia v1.1.0 // indirect
	github.com/astaxie/beego v1.12.0
	github.com/baiyubin/aliyun-sts-go-sdk v0.0.0-20180326062324-cfa1a18b161f // indirect
	github.com/boombuler/barcode v1.0.0
	github.com/bradfitz/gomemcache v0.0.0-20190913173617-a41fca850d0b // indirect
	github.com/casbin/casbin v1.9.1 // indirect
	github.com/couchbase/go-couchbase v0.0.0-20191217190632-b2754d72cc98 // indirect
	github.com/couchbase/gomemcached v0.0.0-20191004160342-7b5da2ec40b2 // indirect
	github.com/couchbase/goutils v0.0.0-20191018232750-b49639060d85 // indirect
	github.com/creack/pty v1.1.9 // indirect
	github.com/edsrzf/mmap-go v1.0.0 // indirect
	github.com/go-redis/redis v6.15.6+incompatible // indirect
	github.com/go-sql-driver/mysql v1.4.1
	github.com/gogo/protobuf v1.3.1 // indirect
	github.com/golang/protobuf v1.3.2 // indirect
	github.com/golang/snappy v0.0.1 // indirect
	github.com/huichen/sego v0.0.0-20180617034105-3f3c8a8cfacc
	github.com/issue9/assert v1.3.4 // indirect
	github.com/kardianos/service v1.0.0
	github.com/kr/pretty v0.2.0 // indirect
	github.com/kr/pty v1.1.8 // indirect
	github.com/lib/pq v1.3.0 // indirect
	github.com/lifei6671/gocaptcha v0.0.0-20190301083731-c467a25bc100
	github.com/mattn/go-sqlite3 v2.0.2+incompatible // indirect
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	github.com/onsi/ginkgo v1.11.0 // indirect
	github.com/onsi/gomega v1.8.1 // indirect
	github.com/pelletier/go-toml v1.6.0 // indirect
	github.com/pkg/errors v0.8.1 // indirect
	github.com/russross/blackfriday v2.0.0+incompatible
	github.com/satori/go.uuid v1.2.0 // indirect
	github.com/siddontang/ledisdb v0.0.0-20190202134119-8ceb77e66a92 // indirect
	github.com/stretchr/objx v0.2.0 // indirect
	github.com/syndtr/goleveldb v1.0.0 // indirect
	github.com/wendal/errors v0.0.0-20181209125328-7f31f4b264ec // indirect
	golang.org/x/crypto v0.0.0-20191227163750-53104e6ec876 // indirect
	golang.org/x/image v0.0.0-20191214001246-9130b4cfad52 // indirect
	golang.org/x/net v0.0.0-20191209160850-c0dbc17a3553 // indirect
	golang.org/x/sync v0.0.0-20190911185100-cd5d95a43a6e // indirect
	golang.org/x/sys v0.0.0-20200102141924-c96a22e43c9c // indirect
	golang.org/x/time v0.0.0-20191024005414-555d28b269f0 // indirect
	golang.org/x/tools v0.0.0-20200102200121-6de373a2766c // indirect
	golang.org/x/xerrors v0.0.0-20191204190536-9bdfabe68543 // indirect
	google.golang.org/appengine v1.6.5 // indirect
	gopkg.in/asn1-ber.v1 v1.0.0-20181015200546-f715ec2f112d // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/ldap.v2 v2.5.1
	gopkg.in/yaml.v2 v2.2.7 // indirect
)
